CREATE TABLE `t_slowlog` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `useDb` varchar(100) DEFAULT NULL,
    `time` varchar(255) DEFAULT NULL,
    `timestampStr` varchar(255) DEFAULT NULL,
    `user` varchar(255) DEFAULT NULL,
    `ip` varchar(255) DEFAULT NULL,
    `queryTime` varchar(255) DEFAULT NULL,
    `lockTime` varchar(255) DEFAULT NULL,
    `rowSent` varchar(255) DEFAULT NULL,
    `rowExamined` varchar(255) DEFAULT NULL,
    `sqlString` text DEFAULT NULL,
    `tableName` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;