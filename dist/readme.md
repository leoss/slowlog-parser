## 说明
slowlog 转sql语句使用说明

### 配置文件

> 数据库连接字符
datasource: system:8JepNdi1dWVu@tcp(10.68.7.127:33306)/clog

> 待解析slowlog日志文件地址（相对脚本路径）
logpath: slow.log

### 存放数据库表结构

> 找个库执行一下

CREATE TABLE `t_slowlog` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `useDb` varchar(100) DEFAULT NULL,
    `time` varchar(255) DEFAULT NULL,
    `timestampStr` varchar(255) DEFAULT NULL,
    `user` varchar(255) DEFAULT NULL,
    `ip` varchar(255) DEFAULT NULL,
    `queryTime` varchar(255) DEFAULT NULL,
    `lockTime` varchar(255) DEFAULT NULL,
    `rowSent` varchar(255) DEFAULT NULL,
    `rowExamined` varchar(255) DEFAULT NULL,
    `sqlString` text DEFAULT NULL,
    `tableName` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;