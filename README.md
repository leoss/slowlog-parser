# slowlogParser

#### 介绍
mysql的慢查询日志解析工具，输出到一个数据的表里边



### 日志解析

原日志格式：

    /opt/bitnami/mysql/bin/mysqld, Version: 5.7.26-log (MySQL Community Server (GPL)). started with:
    Tcp port: 3306  Unix socket: /opt/bitnami/mysql/tmp/mysql.sock
    Time                 Id Command    Argument

    # Time: 2021-06-26T00:00:05.250595+08:00
    # User@Host: calarm[calarm] @  [10.244.0.176]  Id: 405911
    # Query_time: 4.977888  Lock_time: 0.000123 Rows_sent: 1  Rows_examined: 15973877
    use calarm;
    SET timestamp=1624636805;
    select count(1) FROM msg_info where trigger_time<date_add(DATE_FORMAT(CURDATE(),'%Y-%m-%d %H:%i:%s'), interval -1 DAY);
    # Time: 2021-06-26T00:00:08.236660+08:00
    # User@Host: calarm[calarm] @  [10.244.0.176]  Id: 405815
    # Query_time: 2.170010  Lock_time: 0.000138 Rows_sent: 0  Rows_examined: 100000
    SET timestamp=1624636808;
    delete FROM msg_info where trigger_time<date_add(DATE_FORMAT(CURDATE(),'%Y-%m-%d %H:%i:%s'), interval -1 DAY) limit 100000;

解析过后的数据库存放：

![](md_images/2021-07-01-12-36-31.png)


### 配置文件

> 数据库连接字符

    datasource: system:8JepNdi1dWVu@tcp(10.68.7.127:33306)/clog
                {username}:{password}@tcp(host:port)/{dbName}

> 待解析slowlog日志文件地址（相对脚本路径）

    logpath: slow.log

### 存放数据库表结构

> 找个库执行一下


```
CREATE TABLE `t_slowlog` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `useDb` varchar(100) DEFAULT NULL,
    `time` varchar(255) DEFAULT NULL,
    `timestampStr` varchar(255) DEFAULT NULL,
    `user` varchar(255) DEFAULT NULL,
    `ip` varchar(255) DEFAULT NULL,
    `queryTime` varchar(255) DEFAULT NULL,
    `lockTime` varchar(255) DEFAULT NULL,
    `rowSent` varchar(255) DEFAULT NULL,
    `rowExamined` varchar(255) DEFAULT NULL,
    `sqlString` text DEFAULT NULL,
    `tableName` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```

### 整体使用结构

如dist文件夹内

    执行程序  slowlogParser
    配置文件  parse.yml
    日志文件  slow.log


