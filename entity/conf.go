package entity

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Config struct {
	Datasource string `yaml:"datasource"`
	Logpath string `yaml:"logpath"`
}

func (c *Config) GetConf() *Config {
	yamlFile, err := ioutil.ReadFile("parse.yml")
	if err != nil {
		fmt.Println(err.Error())
	}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		fmt.Println(err.Error())
	}
	return c
}

func NewConfig() *Config {
	return &Config{}
}